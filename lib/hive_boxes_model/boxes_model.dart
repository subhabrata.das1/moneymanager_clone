import 'package:hive/hive.dart';
import 'package:moneymanager_clone/constants/app_strings.dart';
import 'package:moneymanager_clone/hive_models/income_model.dart';

class Boxes {
  static Box<IncomeModel> getData() => Hive.box<IncomeModel>(income);
  static Box<SetLimitModel> getLimit() => Hive.box<SetLimitModel>(setlimit);
}
