// ignore_for_file: prefer_const_constructors, must_be_immutable, file_names

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../views/more_page.dart';
import '../views/home_page.dart';
import '../views/status_page.dart';
import '../views/budget_page.dart';

final selectedTabIndexProvider = StateProvider<int>((ref) => 0);

class MyBottomNavBar extends ConsumerWidget {
  MyBottomNavBar({
    Key? key,
  }) : super(key: key);

  List<Widget> pages = [
    HomePage(),
    BudgetPage(),
    StatusPage(),
    MorePage(),
  ];
  int ind = 0;

  @override
  Widget build(BuildContext context, ref) {
    final selectedTabIndex = ref.watch(selectedTabIndexProvider);
    return Scaffold(
      bottomNavigationBar: NavigationBar(
        surfaceTintColor: Colors.white,
        onDestinationSelected: (int index) {
          ind = index;
          ref.read(selectedTabIndexProvider.notifier).state = index;
        },
        selectedIndex: selectedTabIndex < 4 ? selectedTabIndex : ind,
        destinations: const <Widget>[
          NavigationDestination(
            selectedIcon: ImageIcon(
                AssetImage("assets/navigation_menu/ic_menu_home_selected.png")),
            icon: ImageIcon(
                AssetImage("assets/navigation_menu/ic_menu_home.png")),
            label: "Home",
          ),
          NavigationDestination(
            selectedIcon: ImageIcon(AssetImage(
                "assets/navigation_menu/ic_menu_vision_selected.png")),
            icon: ImageIcon(
                AssetImage("assets/navigation_menu/ic_menu_vision.png")),
            label: "Budget",
          ),
          NavigationDestination(
            selectedIcon: ImageIcon(AssetImage(
                "assets/navigation_menu/ic_menu_profile_selected.png")),
            icon: ImageIcon(
                AssetImage("assets/navigation_menu/ic_menu_profile.png")),
            label: "Status",
          ),
          NavigationDestination(
            selectedIcon: ImageIcon(AssetImage(
                "assets/navigation_menu/ic_menu_setting_selected.png")),
            icon: ImageIcon(
                AssetImage("assets/navigation_menu/ic_menu_setting.png")),
            label: "More",
          ),
        ],
      ),
      body: IndexedStack(
        index: selectedTabIndex,
        children: pages,
      ),
    );
  }
}
