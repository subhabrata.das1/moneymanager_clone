import 'package:hive/hive.dart';
part 'income_model.g.dart';

@HiveType(typeId: 0)
class IncomeModel extends HiveObject {
  @HiveField(0)
  String category;
  @HiveField(1)
  String date;
  @HiveField(2)
  String time;
  @HiveField(3)
  String amount;
  @HiveField(4)
  String type;

  IncomeModel(
      {required this.category,
      required this.date,
      required this.time,
      required this.amount,
      required this.type});
}

@HiveType(typeId: 1)
class SetLimitModel extends HiveObject {
  @HiveField(0)
  String category;
  @HiveField(1)
  String limit;

  SetLimitModel({
    required this.category,
    required this.limit,
  });
}
