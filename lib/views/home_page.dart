// ignore_for_file: sort_child_properties_last, prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moneymanager_clone/constants/app_strings.dart';
import 'package:moneymanager_clone/hive_boxes_model/boxes_model.dart';
import 'package:moneymanager_clone/hive_models/income_model.dart';
import 'package:moneymanager_clone/theme/text_styles.dart';
import 'package:moneymanager_clone/views/add_page.dart';

class HomePage extends ConsumerStatefulWidget {
  const HomePage({super.key});

  @override
  ConsumerState<HomePage> createState() => _HomePageState();
}

class _HomePageState extends ConsumerState<HomePage> {
  int inCome = 0, expence = 0;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Boxes.getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          flexibleSpace: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "ALL TIME",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Container(
                  alignment: Alignment.center,
                  height: 30,
                  width: 75,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        "All",
                        style: TextStyle(fontSize: 17),
                      ),
                      Icon(Icons.arrow_drop_down_outlined)
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.grey[300]),
                ),
              ],
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: ValueListenableBuilder<Box<IncomeModel>>(
              valueListenable: Boxes.getData().listenable(),
              builder: (context, box, _) {
                inCome = 0;
                expence = 0;
                final value = box.values.toList().cast<IncomeModel>();
                for (var i in value) {
                  if (i.type == income) {
                    inCome = inCome + int.parse(i.amount);
                  }
                  if (i.type == expense) {
                    expence = expence + int.parse(i.amount);
                  }
                }
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              padding: EdgeInsets.all(12),
                              height: 65,
                              width: 160,
                              decoration: BoxDecoration(
                                color: Colors.grey[300],
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        income,
                                        style: TextStyle(
                                            fontSize: 17, color: Colors.grey),
                                      ),
                                      Text(inCome.toString()),
                                    ],
                                  ),
                                  CircleAvatar(
                                    radius: 12,
                                    child: Icon(
                                      Icons.arrow_back_rounded,
                                      size: 15,
                                    ),
                                    backgroundColor: Colors.green[100],
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(12),
                              height: 65,
                              width: 160,
                              decoration: BoxDecoration(
                                color: Colors.grey[300],
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        expense,
                                        style: TextStyle(
                                            fontSize: 17, color: Colors.grey),
                                      ),
                                      Text(expence.toString())
                                    ],
                                  ),
                                  CircleAvatar(
                                    radius: 12,
                                    child: Icon(
                                      Icons.arrow_forward_rounded,
                                      size: 15,
                                      color: Colors.orangeAccent,
                                    ),
                                    backgroundColor: Colors.orange[50],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        value.isEmpty
                            ? Column(
                                children: [
                                  SvgPicture.asset(
                                    "assets/navigation_menu/undraw_receipt_re_fre3.svg",
                                    height: 400,
                                  ),
                                  Text(
                                    "No Transaction",
                                    style: TextStyle(
                                        fontSize: 23, letterSpacing: .5),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    "Tap + to add new expense/inCome",
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.grey,
                                        letterSpacing: .5),
                                  )
                                ],
                              )
                            : SizedBox(
                                height: size.height * .55,
                                child: ListView.builder(
                                  itemCount: value.length,
                                  itemBuilder: (context, index) {
                                    return ListTile(
                                      title: Row(
                                        children: [
                                          Text(
                                            value[index].category,
                                            style: commonBlackText20,
                                          ),
                                          Spacer(),
                                          Text(
                                            value[index].type == income
                                                ? "+${value[index].amount}"
                                                : "-${value[index].amount}",
                                            style: value[index].type == income
                                                ? incomeGreenText20
                                                : expenceRedText20,
                                          ),
                                        ],
                                      ),
                                      trailing: IconButton(
                                          onPressed: () {
                                            value[index].delete();
                                          },
                                          icon: Icon(Icons.delete)),
                                    );
                                  },
                                ),
                              )
                      ],
                    ),
                  ),
                );
              }),
        ),
        floatingActionButton: FloatingActionButton(
          heroTag: null,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddPage()),
            );
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
