import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:moneymanager_clone/constants/app_strings.dart';
import 'package:moneymanager_clone/hive_models/income_model.dart';
import 'package:path_provider/path_provider.dart';

import 'bottomNav/bottomNav.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var directory = await getApplicationDocumentsDirectory();
  Hive.init(directory.path);
  Hive.registerAdapter(IncomeModelAdapter());
  await Hive.openBox<IncomeModel>(income);
  Hive.registerAdapter(SetLimitModelAdapter());
  await Hive.openBox<SetLimitModel>(setlimit);
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Money Monitor',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyBottomNavBar(),
    );
  }
}
